# New script to begin evaluating how to run analyses against a neutral competitor. 


#' a script to run real landscape based simulations on a supercomputing cluster


repnum=300 #Number of replicate simulations to be conducted for each 
multi_sub=1
# scriptnum=0

strategy_types=c("neutral","fixed_spreader","fixed_dominator","flex_spreader","flex_dominator")
seedvartypes=c("var")#,"even"

sp1_strats=c("neutral","neutral","neutral","neutral")#,"fixed_dominator","fixed_spreader","flex_dominator","flex_spreader")#"fixed_dominator"#
sp2_strats=c("fixed_dominator","fixed_spreader","flex_dominator","flex_spreader")#,"neutral","neutral","neutral","neutral")

scenarios=paste0(sp1_strats,"-",sp2_strats)


compvals=rep(1,length(sp2_strats))


gridscript=readLines("land_comp_sim_script.R")
mem_lines=c('#SBATCH --mem=32000')
time_lines=c('#SBATCH --time=4:00:00')

rmline="#rm(list=ls(all=TRUE))" 



#' Run setup sections

scriptnum=0
for (zz in 1:length(scenarios)){
  for (cc in 1:length(seedvartypes)){ 
    for (xx in 1:repnum){
      scriptnum=scriptnum+1
      plastic_strategies_val=FALSE
      if(sp1_strats[zz]=="flex_dominator"|sp2_strats[zz]=="flex_dominator"|sp1_strats[zz]=="flex_spreader"|sp2_strats[zz]=="flex_spreader"){plastic_strategies_val=TRUE}
      
      
      #Create R scripts to run each condition
      parval=paste("rep=",xx,sep="")
      
      
      sp1_val=paste("biastypesp1='",sp1_strats[zz],"'",sep="")
      sp2_val=paste("biastypesp2='",sp2_strats[zz],"'",sep="")
      compval=paste("compvals=",compvals[zz],sep="")
      scenarioval=paste("scenario='",scenarios[zz],"'",sep="")
      varseedval=paste("varseed='",seedvartypes[cc],"'",sep="")
      plastic_val=paste("plastic_strategies=",plastic_strategies_val,sep="")
      
      parscript=c(rmline,parval,sp1_val,sp2_val,plastic_val,compval,scenarioval,varseedval,gridscript)
      write(parscript,file=paste("Scripter/land_comp_",scenarios[zz],seedvartypes[cc],"rep",xx,".R",sep=''))
    
    
   
      pbslines=c('#!  /bin/bash',
                 mem_lines,
                 time_lines,
                 '#SBATCH --nodes=1',
                 paste0('#SBATCH --job-name=','land_comp_',scenarios[zz],'_rep',xx),
                 'export MKL_NUM_THREADS="36"',
                 'module load r',
                 'cd ~/Dispersal_strategies/',
                 paste(' R CMD BATCH --vanilla ~/Dispersal_strategies/src/Scripter/land_comp_',scenarios[zz],seedvartypes[cc],'rep',xx,'.R ','~/Dispersal_strategies/src/Scripter/land_comp_',scenarios[zz],seedvartypes[cc],'rep',xx,'.Rout' ,sep=''))
        
        
        
      
      
    
      
      write(pbslines,file=paste('Scripter/land_comp_',scenarios[zz],seedvartypes[cc],"rep",xx,'.PBS',sep=''))
      
      system(paste(shQuote("sbatch"), ' Scripter/land_comp_',scenarios[zz],seedvartypes[cc],"rep",xx,'.PBS',sep=''))
      # print(paste(shQuote("qsub"), ' Scripter/land_comp_',scenarios[zz],seedvartypes[cc],"rep",pp,'.PBS',sep=''))
    }
  }
  
}
# }  
#   



