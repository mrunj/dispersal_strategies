#' a script to run real landscape based simulations on a supercomputing cluster

x=system("domainname",intern=TRUE)
msicluster=Sys.getenv(x = "MSICLUSTER")
hostname=system("hostname",intern=TRUE)

repnum=50 #Number of replicate simulations to be conducted for each 
scriptnum=0
inv_strategies=c("flex_spreader","flex_dominator")
strategy_types=c("neutral","fixed_spreader","fixed_dominator","flex_spreader","flex_dominator")
strategycodes=c("n","is","id","ls","ld")

scenarios=c("fixspr-fixdom","flexspr-flexdom","flexspr-neu","flexdom-neu")
sp1_strats=c("fixed_spreader","flex_spreader","neutral","neutral")
sp2_strats=c("fixed_dominator","flex_dominator","flex_spreader","flex_dominator")
compvals=c(1,1,.9,.9)


gridscript=readLines("multi_strategy_cluster_script.R")

rmline="rm(list=ls(all=TRUE))" 



  #' Run setup sections
  for (xx in 1:repnum){
#       for (zz in 1:length(inv_strategies)){
#         for(tt in 1:length(strategy_types)){
    for (zz in 1:length(scenarios)){
    
    scriptnum=scriptnum+1
  
    
    #Create R scripts to run each condition
    parval=paste("rep=",xx,sep="")
#     compval=paste("compvals='",strategy_types[tt],"'",sep="")
#     stratval=paste("biastypesp2='",inv_strategies[zz],"'",sep="")
#     
#     displine=paste("simple_disperse=",simple_disperse,sep="")
#     randline=paste("randomizehabs=",randomizehabs,sep="")

    sp1_val=paste("biastypesp1='",sp1_strats[zz],"'",sep="")
    sp2_val=paste("biastypesp2='",sp2_strats[zz],"'",sep="")
    compval=paste("compvals=",compvals[zz],sep="")
    scenarioval=paste("scenario='",scenarios[zz],"'",sep="")

    
parscript=c(rmline,parval,sp1_val,sp2_val,compval,scenarioval,gridscript)
write(parscript,file=paste("Scripter/lab",scenarios[zz],"rep",xx,".R",sep=''))
    
    #Create submission scripts for each R script
    cores_per_node=16
    pbslines=c('#!  /bin/bash',
           '#PBS -l walltime=50:00:00,pmem=2750mb,nodes=1:ppn=16',
           '#PBS -m abe',
           '#PBS -M mrunj@umn.edu',
           'cd ~/Dispersal_strategies/',
           paste('R CMD BATCH ~/Dispersal_strategies/src/Scripter/lab',scenarios[zz],'rep',xx,'.R ','~/Dispersal_strategies/src/Scripter/lab',scenarios[zz],'rep',xx,'.Rout',sep=''))

           write(pbslines,file=paste('Scripter/lab',scenarios[zz],'rep',xx,'.PBS',sep='')) 
           # system(paste(shQuote("qsub"), '-q lab-long Scripter/lab',scenarios[zz],'rep',xx,'.PBS',sep=''))
      }
    
  }
# }  
#   
  
    
      
      
      
      
