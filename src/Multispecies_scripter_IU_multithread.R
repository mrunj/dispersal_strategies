# New script to begin evaluating how to run analyses against a neutral competitor. 


#' a script to run real landscape based simulations on a supercomputing cluster

x=system("domainname",intern=TRUE)
msicluster=Sys.getenv(x = "MSICLUSTER")
hostname=system("hostname",intern=TRUE)

repnum=30 #Number of replicate simulations to be conducted for each 
multi_sub=15
# scriptnum=0
species_set_vals=c(2,5,10,100,200)
plastic_strategies_vals=c(FALSE,TRUE)
plasticity_types=c("fixed","flex")
#' Run setup sections
gridscript=readLines("Multispecies_cluster_sim_script.R")

core_nums=c(16,16,16,16,16)
resource_lines=c('#PBS -l walltime=24:00:00,mem=32gb,nodes=1:ppn=16',
                 '#PBS -l walltime=24:00:00,mem=32gb,nodes=1:ppn=16',
                 '#PBS -l walltime=24:00:00,mem=32gb,nodes=1:ppn=16',
                 '#PBS -l walltime=120:00:00,mem=62gb,nodes=1:ppn=16',
                 '#PBS -l walltime=200:00:00,mem=62gb,nodes=1:ppn=16')

rmline="rm(list=ls(all=TRUE))" 

for (nn in 1:5){#length(species_set_vals)){
  for(ff in 1:length(plastic_strategies_vals)){
    for (xx in 1:repnum){
      
      # scriptnum=scriptnum+1
      
      #Create R scripts to run each condition
      parval=paste("rep=",xx,sep="")
      species_set_val=paste("speciesnum=",species_set_vals[nn],sep="")
      plastic_val=paste("plastic_strategies=",plastic_strategies_vals[ff],sep="")
      
      parscript=c(rmline,parval,plastic_val,species_set_val,gridscript)
      write(parscript,file=paste("Scripter/multispecies_",plasticity_types[ff],"_",species_set_vals[nn],"_species_rep",xx,".R",sep=''))
    }
    
    for (pp in seq.int(from=1,to=repnum,by=multi_sub)){
      #Create submission scripts for each R script
      cores_per_node=core_nums[nn]
      pbslines=c('#!  /bin/bash',
                 resource_lines[nn],
                 '#PBS -m abe',
                 '#PBS -M mrunj@iu.edu',
                 '#PBS -q cpu',
                 'module load openssl/1.0.2k',
                 'module load r/intel/3.4.4',
                 'module load pcp/2008',
                 'cd ~/Dispersal_strategies/',
                 paste('aprun -n ',cores_per_node, ' pcp src/Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp,'.txt',sep=""))

      process_lines=vector()
      for (uu in 0:(multi_sub-1)){
        
        process_lines=c(process_lines,paste('R CMD BATCH ~/Dispersal_strategies/src/Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp+uu,'.R ','~/Dispersal_strategies/src/Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp+uu,'.Rout',sep=''))
        
      }
    
      # process_lines=c(process_lines,paste('R CMD BATCH ~/Dispersal_strategies/src/Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp+uu+1,'.R ','~/Dispersal_strategies/src/Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp+uu+1,'.Rout',sep=''))
      
      # process_lines=c(process_lines,"wait")
      
      
      
      write(pbslines,file=paste('Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp,'.PBS',sep=''))
      write(process_lines,file=paste('Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp,'.txt',sep=''))
      
      system(paste(shQuote("qsub"), ' Scripter/multispecies_',plasticity_types[ff],"_",species_set_vals[nn],'_species_rep',pp,'.PBS',sep=''))
      
    }
  }
  
  
}
