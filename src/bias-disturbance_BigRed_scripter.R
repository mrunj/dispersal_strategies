# New script to begin evaluating how to run analyses against a neutral competitor. 


#' a script to run real landscape based simulations on a supercomputing cluster

x=system("domainname",intern=TRUE)
msicluster=Sys.getenv(x = "MSICLUSTER")
hostname=system("hostname",intern=TRUE)

repnum=10 #Number of replicate simulations to be conducted for each 
multi_sub=1
scriptnum=0
inv_strategies=c("flex_spreader","flex_dominator")
strategy_types=c("neutral","fixed_spreader","fixed_dominator","flex_spreader","flex_dominator")
strategycodes=c("n","is","id","ls","ld")
seedvartypes=c("var")#,"even"

sp1_strats=c("neutral","neutral","neutral","neutral","fixed_dominator","fixed_spreader","flex_dominator","flex_spreader")#"fixed_dominator"#
sp2_strats=c("fixed_dominator","fixed_spreader","flex_dominator","flex_spreader","neutral","neutral","neutral","neutral")

scenarios=paste0(sp1_strats,"-",sp2_strats)

compvals=rep(1,length(sp2_strats))


gridscript=readLines("bias-disturbance_sim_script.R")

rmline="#rm(list=ls(all=TRUE))" 



#' Run setup sections

for (zz in 1:length(scenarios)){
  for (cc in 1:length(seedvartypes)){ 
    for (xx in 1:repnum){
      
      scriptnum=scriptnum+1
      plastic_strategies_val=FALSE
      if(sp1_strats[zz]=="flex_dominator"|sp2_strats[zz]=="flex_dominator"|sp1_strats[zz]=="flex_spreader"|sp2_strats[zz]=="flex_spreader"){plastic_strategies_val=TRUE}
      
      #Create R scripts to run each condition
      parval=paste("rep=",xx,sep="")
      
      
      sp1_val=paste("biastypesp1='",sp1_strats[zz],"'",sep="")
      sp2_val=paste("biastypesp2='",sp2_strats[zz],"'",sep="")
      compval=paste("compvals=",compvals[zz],sep="")
      scenarioval=paste("scenario='",scenarios[zz],"'",sep="")
      varseedval=paste("varseed='",seedvartypes[cc],"'",sep="")
      plastic_val=paste("plastic_strategies=",plastic_strategies_val,sep="")
      
      parscript=c(rmline,parval,sp1_val,sp2_val,compval,scenarioval,varseedval,plastic_val,gridscript)
      write(parscript,file=paste("Scripter/bias_",scenarios[zz],seedvartypes[cc],"rep",xx,".R",sep=''))
    
    
 
      
      #Create submission scripts for each R script
      cores_per_node=16
      pbslines=c('#!  /bin/bash',
                 '#PBS -l walltime=12:00:00,mem=32gb,nodes=1:ppn=16',
                 '#PBS -m abe',
                 '#PBS -M mrunj@iu.edu',
                 '#PBS -l gres=ccm',
                 '#PBS -V ',
                 'module load ccm',
                 'cd ~/Dispersal_strategies/',
                 paste('ccmrun R CMD BATCH  ~/Dispersal_strategies/src/Scripter/bias_',scenarios[zz],seedvartypes[cc],'rep',xx,'.R ','~/Dispersal_strategies/src/Scripter/bias_',scenarios[zz],seedvartypes[cc],'rep',xx,'.Rout',sep=''))


      write(pbslines,file=paste('Scripter/bias_',scenarios[zz],seedvartypes[cc],"rep",xx,'.PBS',sep=''))
      system(paste(shQuote("qsub"), ' Scripter/bias_',scenarios[zz],seedvartypes[cc],"rep",xx,'.PBS',sep=''))
    }
  }
  
}
# }  
#   




# 
# 
# setwd("..")

# slist=list.files("src/Scripter/",pattern = "bias")
# slist=slist[grep(".R",slist)]
# for(ff in 1:length(slist)){
# source(paste("src/Scripter/",slist[ff],sep=''))
# }