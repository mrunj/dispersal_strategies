#' a script to run real landscape based simulations on a supercomputing cluster

x=system("domainname",intern=TRUE)
msicluster=Sys.getenv(x = "MSICLUSTER")
hostname=system("hostname",intern=TRUE)

repnum=20 #Number of replicate simulations to be conducted for each 
scriptnum=0

strategy_types=c("neutral","fixed_spreader","fixed_dominator","flex_spreader","flex_dominator")
strategycodes=c("n","is","id","ls","ld")


gridscript=readLines("multi_strategy_cluster_script.R")

rmline="rm(list=ls(all=TRUE))" 



  #' Run setup sections
  for (xx in 1:repnum){
    scriptnum=scriptnum+1
    for(tt in 1:length(strategy_types)){
    
    
    #Create R scripts to run each condition
    parval=paste("rep=",xx,sep="")
    compval=paste("biastypesp1='",strategy_types[tt],"'",sep="")
    
#     
#     displine=paste("simple_disperse=",simple_disperse,sep="")
#     randline=paste("randomizehabs=",randomizehabs,sep="")
    
parscript=c(rmline,parval,compval,gridscript)
write(parscript,file=paste('Scripter/spread_',strategycodes[tt],'_block_rep',xx,'.R',sep=''))
    
    #Create submission scripts for each R script
    cores_per_node=24
    pbslines=c('#!  /bin/bash',
           '#PBS -l walltime=8:00:00,pmem=2580mb,nodes=1:ppn=24',
           '#PBS -m abe',
           '#PBS -M mrunj@umn.edu',
           'cd ~/Dispersal_strategies/',
           paste('R CMD BATCH ~/Dispersal_strategies/src/Scripter/spread_',strategycodes[tt],'_block_rep',xx,'.R ~/Dispersal_strategies/src/Scripter/spread_',strategycodes[tt],'_block_rep',xx,'.Rout',sep=''))

           write(pbslines,file=paste('Scripter/spread_',strategycodes[tt],'_block_run',xx,'.PBS',sep='')) 
           system(paste(shQuote("qsub"), ' Scripter/spread_',strategycodes[tt],'_block_run',xx,'.PBS',sep=''))
    }
  }
  
  
  
    
      
      
      
      