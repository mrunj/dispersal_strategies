# load libraries
library(fields)
library(snowfall)

source("src/multi_strategy_even_seed_dispsim_func.R")

showtimes=seq(1,1000,by=4)


# Basic system parameters

steps=1000
sidelength=50
totalcells=sidelength^2
speciesnum=2

landlevs=seq(0.5,1,length=1) #habitat heterogeneity, via Hyunt factor for clumpiness
biasvals=seq(0.75,1,length=1) #flexibility in biasing seeds based on resources; 0 flexibility means seeds are always the same size, higher values means resources more strongly determine size:number ratio
# biastypesp1=c("neutral","fixed_spreader","fixed_dominator")#,"flex_spreader","flex_dominator") #size bias can be either a "spreader" or "dominator" strategy
# biastypesp2="flex_spreader"

seedprodvals=c(20) #seed production values for sp 2, sp 1 is set at 20
# compvals= c(.9) #competition coefficient values for sp 2, sp 1 is set at 1
resvarlevs=c(1)#c(.5,.75,1,1.25,1.5) #value that scales the variability in the resource landscape to more or less even than the standard created by altland. A non-spatial analaog to the variability produced from landlevs. 


inittype="front" #"block", "random" or "fractal"
blocksize=2 #Size of initial population block
blockpos="center" #"center" vs "corner"
hinit=.5 #Hyunt factor to determine clumpiness of initial populations
pinit=1 #proportion of habitat that has positive values of initial populations

resreplace="fractal" #"constant", "random" or "fractal"
maxprod=seq(110,200,length=1) #Resource production rate if constant, maximal production rate if quality varies
pool_size=1.5 #What is the amount of additional resources that can be stored beyond the production rate as a multiple of the production rate. This will also determine the relative quality value to determine seed size bias


pres=seq(.5,1,length=1) #proportion of habitat that has positive values of resource production for fractal resources
seed=sample(1000,size=1)#random seed 

# r0s=c(1.2,1.3) #intrinsic rate of increase for each species as a vector
seedmort1=.1
seedmort2=.1
util1=5
util2=5
util=c(util1,util2) #average amount of resources used by an individual of each species
utildev=.1   #deviation in average amount of resources used by an individual of each species
mucoefs1=100
mucoefs2=100     
sigmacoefs1=5
sigmacoefs2=5
MPP=150 # Meters per pixel


disturb=TRUE #do you allow regular disturbance (at each time step)
disturb_type='random' #are disturbed patches randomly assigned across the landscape or clumped
disturb_extent=c(0,.05,.1,.15,.2,.25,.3) #what fraction of the landscape is disturbed in each time step



serial=TRUE
simserial=FALSE

source("src/disperser_func.R")
source("src/dispersal_mats.R")

cpunum=7

plotflag=FALSE
###############

parset=expand.grid(steps=steps, sidelength=sidelength, speciesnum=speciesnum, inittype=inittype, blocksize=blocksize, blockpos=blockpos, hinit=hinit, pinit=pinit, resreplace=resreplace, maxprod=maxprod, pool_size=pool_size, hres=landlevs, pres=pres, resscaler=resvarlevs, seed=seed, MPP=MPP, seedprod=seedprodvals, seedmort1=seedmort1, seedmort2=seedmort2, compcoef2=compvals, util2=util2, utildev=utildev,biastypesp1=biastypesp1, biastypesp2=biastypesp2, biasvals=biasvals, mucoefs1=mucoefs1, mucoefs2=mucoefs2, sigmacoefs1=sigmacoefs1,sigmacoefs2=sigmacoefs2, disturb=disturb, disturb_type=disturb_type, disturb_extent=disturb_extent)




landcomp=array(dim=c(steps+1,speciesnum,nrow(parset))) #an array that stores the average population across the landscape

domcomp=array(dim=c(nrow(parset),2)) #an array that stores the ratio of the different species averaged across the landscape [,,1] and the total plant population size[,,2]



ptm=proc.time()

##############         

runsim=function(pp,parset){
  write(pp,paste("src/Scripter/doneset/",scenario,"rep",rep,"_par", pp,".txt",sep=''))
  
  steps=parset[pp,"steps"]
  sidelength=parset[pp,"sidelength"]
  speciesnum=parset[pp,"speciesnum"]
  inittype=parset[pp,"inittype"] #"block", "random" or "fractal"
  blocksize=parset[pp,"blocksize"] #Size of initial population block
  blockpos=parset[pp,"blockpos"]#"center" #"center" vs "corner"
  hinit=parset[pp,"hinit"] #Hyunt factor to determine clumpiness of initial populations
  pinit=parset[pp,"pinit"] #proportion of habitat that has positive values of initial populations
  
  resreplace=parset[pp,"resreplace"] #"constant", "random" or "fractal"
  maxprod=parset[pp,"maxprod"] #Resource production rate if constant, maximal production rate if quality varies
  pool_size=parset[pp,"pool_size"] #Potential resource pool size as a multiple of production rate
  
  hres=parset[pp,"hres"] #Hyunt factor to determine clumpiness of resource landscape [0,1] for fractal resources
  pres=parset[pp,"pres"] #proportion of habitat that has positive values of resource production for fractal resources
  resscaler=parset[pp,"resscaler"] #value that scales the variability in the resource landscape to more or less even than the standard created by altland
  seed=parset[pp,"seed"]#random seed 
  
  # r0s=c(1.2,1.3) #intrinsic rate of increase for each species as a vector
  seedprod=c(20,parset[pp,"seedprod"]) #seed production for each species as a vector
  seedmort=c(parset[pp,"seedmort1"],parset[pp,"seedmort2"]) #natural pre-establishment mortality, without competition
  compcoefs=c(1,parset[pp,"compcoef2"]) #relative competitiveness coefficients for establishment 
  util1=1
  util2=parset[pp,"util2"]
  util=c(util1,util2) #average amount of resources used by an individual of each species
  utildev=parset[pp,"utildev"] #deviation in average amount of resources used by an individual of each species
  biasflex=parset[pp,"biasvals"]#c(0,parset[pp,"biasvals"])
  biastype=c(paste(parset[pp,"biastypesp1"]),paste(parset[pp,"biastypesp2"]))
  mucoefs1=parset[pp,"mucoefs1"]
  mucoefs2=parset[pp,"mucoefs2"]     
  mucoefs=c(mucoefs1,mucoefs2)
  sigmacoefs1=parset[pp,"sigmacoefs1"]
  sigmacoefs2=parset[pp,"sigmacoefs2"]
  sigmacoefs=c(sigmacoefs1,sigmacoefs2)
  MPP=parset[pp,"MPP"] # Meters per pixel
  disturb=parset[pp,"disturb"] #are there disturbances
  disturb_type=parset[pp,"disturb_type"] #spatial arrangement of disturbed patches
  disturb_extent=parset[pp,"disturb_extent"] #how much of the landscape is disturbed in each timepoint
  
  # serial=parset[pp,"serial"] #run in serial
  # cpunum=parset[pp,"cpunum"] #number of processors for parallel run
  # plotflag=plotflag #number of processors for parallel run
  
  
  
  
  
  
  # Basic system parameters
  
  
  
  
  
  simout=dispsim(steps=steps, sidelength=sidelength, speciesnum=speciesnum, inittype=inittype, blocksize=blocksize, blockpos=blockpos, hinit=hinit, pinit=pinit, resreplace=resreplace, maxprod=maxprod, pool_size=pool_size, hres=hres, pres=pres, resscaler=resscaler, seed=seed, seedprod=seedprod, seedmort=seedmort, compcoefs=compcoefs, util=util, utildev=utildev, biastype=biastype, biasflex=biasflex, mucoefs=mucoefs, sigmacoefs=sigmacoefs, MPP=MPP,disturb=disturb, disturb_type=disturb_type, disturb_extent=disturb_extent, serial=serial, cpunum=cpunum,plotflag=plotflag)  
  
  
  return(simout)
}

if(simserial==FALSE){
  sfInit(parallel=TRUE,cpus=cpunum)
  
  sfExportAll()
  pnum=1:nrow(parset)
  out=sfLapply(pnum,fun = runsim,parset)
  
  sfStop()
}
if(simserial==TRUE){
  out=list()
  for (nn in 1:nrow(parset)){
    out[[nn]]=runsim(nn,parset)
  }
}

for (qq in 1:nrow(parset)){
  spmeans=out[[qq]]
  landcomp[,,qq]=t(spmeans)
  domcomp[qq,1]=spmeans[1,steps+1]/spmeans[2,steps+1]
  domcomp[qq,2]=spmeans[1,steps+1]+spmeans[2,steps+1]
  print(qq)
  print(proc.time()-ptm)
}

# Rescale domcomp so that values go from -1, completely dominated by species 1, to +1, completely dominated by species 2
domcomprescale=1/domcomp[,1]
domcomprescale[which(domcomprescale<=1)]=domcomprescale[which(domcomprescale<=1)]-1
domcomprescale[which(domcomprescale>1)]=1-(1/domcomprescale[which(domcomprescale>1)])
print(proc.time()-ptm)

outdata=list(rep,landcomp,domcomp,parset)

save(outdata,file=paste("data/multiscenario_sim/disturb/",scenario,"_",inittype,"_even_seed_rep",rep,".rdata",sep=""))
# save(domcomp,file=paste("data/domcomprep",rep,".rdata",sep=""))
# save.image(file="data/workdata.rdata")

rm(list=ls(all=TRUE))

