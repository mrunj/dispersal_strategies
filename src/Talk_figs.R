library(colorspace)
library(gplots)
library(scales)
 repnum=50
var_par2_name="biasvals"
var_par1_name="disturb_extent"

var_par2=biasvals
var_par1=disturb_extent

scenarios=c("neutral-fixed_dominator", "neutral-fixed_spreader",  "neutral-flex_dominator","neutral-flex_spreader",   "fixed_dominator-neutral", "fixed_spreader-neutral", "flex_dominator-neutral" , "flex_spreader-neutral"  )

dat_files=list.files("data/sandbox/revision_testing/",pattern ="invasibility" )
# dat_files=dat_files[grep(pattern = "reslev_pres_",x = dat_files,invert=FALSE)]
dat_files=dat_files[grep(pattern = "bias_dist_cc_full",x = dat_files,invert=FALSE)]


load(paste0("data/sandbox/revision_testing/",dat_files[1]))
outdata[[4]]
var_par1=unique(outdata[[4]][,var_par1_name])
var_par2=unique(outdata[[4]][,var_par2_name])
steps=unique(outdata[[4]][,"steps"])
multiresp_data=array(dim=c(steps+1,2,length(var_par1)*length(var_par2),length(scenarios),length(dat_files)/length(scenarios),length(seedvartypes)))


# for (cc in 1:length(seedvartypes)){
#   var_files=dat_files[grep(x = dat_files,pattern =seedvartypes[cc] )]
# 
# 
#   for (ss in 1:length(scenarios)){
#     scen_files=var_files[grep(pattern =scenarios[ss],dat_files )]
#     for(dd in 1:repnum){
#       curr_file=scen_files[grep(scen_files,pattern = paste(dd))]
#       load(paste0("data/sandbox/revision_testing/",curr_file))
#       multiresp_data[,,,ss,dd,cc]= outdata[[2]]
#     }
#   }
# }
# 
# 
# 
# par(mfrow=c(length(var_par1),length(var_par2)),mar=c(2,2,2,2))
# for (vv in 1:length(var_par1)){
#   for(uu in 1:length(var_par2)){
#     plot(1:dim(multiresp_data)[1],ylim=c(0,max(multiresp_data,na.rm=TRUE)),type="n",main=paste0(var_par1_name,"=",var_par1[vv],"  ",var_par2_name,"=",var_par2[uu]))
#     # for(cc in 1:length(seedvartypes)){
#     for (ss in 1:length(scenarios)){
#       for(dd in 1:repnum){
#         lines(multiresp_data[,1,which(outdata[[4]][,var_par1_name]==var_par1[vv]&outdata[[4]][,var_par2_name]==var_par2[uu]),ss,dd,1],col=alpha(ss,.6),lty=2)
#         lines(multiresp_data[,2,which(outdata[[4]][,var_par1_name]==var_par1[vv]&outdata[[4]][,var_par2_name]==var_par2[uu]),ss,dd,1],col=alpha(ss,.6),lty=1)
#         # lines(multiresp_data[,1,vv,ss,dd,2],col=alpha(ss+2,.6),lty=2)
#         # lines(multiresp_data[,2,vv,ss,dd,2],col=alpha(ss+2,.6),lty=1)
#       }
#     }
#   }
# }



#######################################################################
# condnameset=c("even_fixspr-fixdom","even_flexspr-flexdom","var_fixspr-fixdom","var_flexspr-flexdom")
condnameset=scenarios
# c("Fixed tradeoff\nConstrained seed production","Flexible tradeoff\nConstrained seed production","Fixed tradeoff\nVariable seed production","Flexible tradeoff\nVariable seed production")
# sepparvals=c("Bias=0", "Bias=0.1", "Bias=0.2", "Bias=0.3", "Bias=0.4", "Bias=0.5", "Bias=0.6", "Bias=0.7", "Bias=0.8", "Bias=0.9")
sepparvals=paste(var_par1)


transient_range=20


# pdf(file="graphs/bias_dist_mut_inv_const_scale_lim_lab.pdf",height=8,width=16)

res_files=list.files("data/sandbox/revision_testing/")

# par(mar=c(4,5.5,4,3),mfcol=c(4,7),cex.lab=1.2)
par(mar=c(2.5,2.5,1,1),mfrow=c(4,length(var_par1)),cex.lab=1.4,cex.axis=1.4)
for(cc in 1:length(seedvartypes)){
  
  var_files=dat_files[grep(x = dat_files,pattern =seedvartypes[cc] )]
  
  
  
  for (ss in 1:length(scenarios[1:4])){
    scen_files=var_files[grep(pattern =scenarios[ss],x = var_files )]
    repnum=length(scen_files)
    
    load(paste0("data/sandbox/revision_testing/",scen_files[1]))
    represcaledata=array(dim=c(dim(outdata[[3]]),repnum))
    reptransrescaledata=array(dim=c(dim(outdata[[3]]),repnum))
    
    #Calculate values for when the non-neutral strategy is the invader
    repTS_strat_inv=array(dim=c(dim(outdata[[2]]),repnum))
    
    
    for(dd in 1:repnum){
      curr_file=scen_files[grep(scen_files,pattern = paste0("rep",dd,".rdata"))]
      load(paste0("data/sandbox/revision_testing/",curr_file))
      # multiresp_data[,,,ss,dd,cc]= outdata[[2]]
      parset=outdata[[4]]
      steps=parset$steps[1]
      
      landcomp=outdata[[2]]
      repTS_strat_inv[,,,dd]=landcomp
      
      transdomcomp=array(dim=c(nrow(parset),2)) #an array that stores the ratio of early year populations of the different species averaged across the landscape 
      domcomp=array(dim=c(nrow(parset),2))
      
      
      #   transpop=t(apply(landcomp[1:transient_range,,],MAR=c(2,3),FUN=mean))#the average population size of each species over the first 20 years
      transpop=t(apply(landcomp[(steps-transient_range):(steps+1),,],MAR=c(2,3),FUN=mean))#the average population size of each species over the first 20 years
      
      
      
      
      for (qq in 1:nrow(parset)){
        
        
        transdomcomp[qq,1]=transpop[qq,1]/transpop[qq,2]
        transdomcomp[qq,2]=transpop[qq,1]+transpop[qq,2]
        #   print(qq)
        #   print(proc.time()-ptm)
      }  
      
      
      transdomcomprescale=1/transdomcomp[,1]
      transdomcomprescale[which(transdomcomprescale<=1)]=transdomcomprescale[which(transdomcomprescale<=1)]-1
      transdomcomprescale[which(transdomcomprescale>1)]=1-(1/transdomcomprescale[which(transdomcomprescale>1)])
      reptransrescaledata[,1,dd]=transdomcomprescale
      reptransrescaledata[,2,dd]=transdomcomp[,2]
      
      
      domcomp=outdata[[3]]
      domcomprescale=1/domcomp[,1]
      domcomprescale[which(domcomprescale<=1)]=domcomprescale[which(domcomprescale<=1)]-1
      domcomprescale[which(domcomprescale>1)]=1-(1/domcomprescale[which(domcomprescale>1)])
      represcaledata[,1,dd]=domcomprescale
      represcaledata[,2,dd]=domcomp[,2]
      
      
      
    }
    
    meanTS_strat_inv=apply(repTS_strat_inv,MAR=c(1,2,3),FUN=mean)
    sdTS_strat_inv=apply(repTS_strat_inv,MAR=c(1,2,3),FUN=sd)
    confTSup_strat_inv=apply(repTS_strat_inv,MAR=c(1,2,3),FUN=quantile,.975)
    confTSlow_strat_inv=apply(repTS_strat_inv,MAR=c(1,2,3),FUN=quantile,.025)
    if(repnum==1){sdTS_strat_inv[]=0}
    
    ##########################
    #Calculate values for when the non-neutral strategy is the invader
    
    
    scen_files=var_files[grep(pattern =scenarios[ss+4],x = var_files )]
    
    load(paste0("data/sandbox/revision_testing/",scen_files[1]))
    represcaledata=array(dim=c(dim(outdata[[3]]),repnum))
    reptransrescaledata=array(dim=c(dim(outdata[[3]]),repnum))
    
    
    repTS_neut_inv=array(dim=c(dim(outdata[[2]]),repnum))
    
    for(dd in 1:repnum){
      curr_file=scen_files[grep(scen_files,pattern = paste0("rep",dd,".rdata"))]
      load(paste0("data/sandbox/revision_testing/",curr_file))
      # multiresp_data[,,,ss,dd,cc]= outdata[[2]]
      parset=outdata[[4]]
      steps=parset$steps[1]
      
      landcomp=outdata[[2]]
      repTS_neut_inv[,,,dd]=landcomp
      
      transdomcomp=array(dim=c(nrow(parset),2)) #an array that stores the ratio of early year populations of the different species averaged across the landscape 
      domcomp=array(dim=c(nrow(parset),2))
      
      
      #   transpop=t(apply(landcomp[1:transient_range,,],MAR=c(2,3),FUN=mean))#the average population size of each species over the first 20 years
      transpop=t(apply(landcomp[(steps-transient_range):(steps+1),,],MAR=c(2,3),FUN=mean))#the average population size of each species over the first 20 years
      
      
      
      
      for (qq in 1:nrow(parset)){
        
        
        transdomcomp[qq,1]=transpop[qq,1]/transpop[qq,2]
        transdomcomp[qq,2]=transpop[qq,1]+transpop[qq,2]
        #   print(qq)
        #   print(proc.time()-ptm)
      }  
      
      
      transdomcomprescale=1/transdomcomp[,1]
      transdomcomprescale[which(transdomcomprescale<=1)]=transdomcomprescale[which(transdomcomprescale<=1)]-1
      transdomcomprescale[which(transdomcomprescale>1)]=1-(1/transdomcomprescale[which(transdomcomprescale>1)])
      reptransrescaledata[,1,dd]=transdomcomprescale
      reptransrescaledata[,2,dd]=transdomcomp[,2]
      
      
      domcomp=outdata[[3]]
      domcomprescale=1/domcomp[,1]
      domcomprescale[which(domcomprescale<=1)]=domcomprescale[which(domcomprescale<=1)]-1
      domcomprescale[which(domcomprescale>1)]=1-(1/domcomprescale[which(domcomprescale>1)])
      represcaledata[,1,dd]=domcomprescale
      represcaledata[,2,dd]=domcomp[,2]
      
      
      
    }
    
    meanTS_neut_inv=apply(repTS_neut_inv,MAR=c(1,2,3),FUN=mean)
    sdTS_neut_inv=apply(repTS_neut_inv,MAR=c(1,2,3),FUN=sd)
    confTSup_neut_inv=apply(repTS_neut_inv,MAR=c(1,2,3),FUN=quantile,.975)
    confTSlow_neut_inv=apply(repTS_neut_inv,MAR=c(1,2,3),FUN=quantile,.025)
    if(repnum==1){sdTS_neut_inv[]=0}
    ######################
    
    
    
    # # To plot just means for all parameters on one plot
    # 
    #   plot(meanTS[1:(steps+1),1,1]+sdTS[1:(steps+1),1,1],type='n',ylim=c(0,20),main=condnameset[ss],xlab="Time", ylab="Average population per patch")
    # 
    #   for(ii in 1:nrow(parset)){
    #     #Strategy 1
    #     lines(meanTS[1:(steps+1),1,ii],type='l',col=diverge_hcl(n = nrow(parset))[ii],lty=3,lwd=2,ylim=c(0,20))
    #     #Strategy 2
    #     lines(meanTS[1:(steps+1),2,ii],type='l',col=diverge_hcl(n = nrow(parset))[ii],lty=1,lwd=1,ylim=c(0,20))
    # 
    # 
    #   }
    # 
    # # to plot means and confidence intervals of each parameter independently. Also have to plot each "scenario" separately (ss values)
    #   pdf(file=paste0("graphs/bias_fig_supp_",ss,".pdf"),width=14,height=6)
    #   par(mar=c(5,5,4,3),mfrow=c(2,5),cex.lab=1.25,cex.main=1.4,cex.axis=1.2)
    #     for(ii in 1:nrow(parset)){
    #          #Strategy 1
    #       plot(smooth.spline(x = 1:(steps+1),y =meanTS[1:(steps+1),1,ii],spar = .50),type='l',col=1,lty=1,lwd=2,ylim=c(0,20),main=substitute(paste( italic(l),"=", ii ),list(ii=sepparvals[ii])),xlab="Time", ylab="Average population per patch")
    #       # lines(confTSup[1:(steps+1),1,ii],type='l',ylim=c(0,20),col=1,lty=2,lwd=1)
    #       lines(smooth.spline(x = 1:(steps+1),y = confTSlow[1:(steps+1),1,ii],spar = .50),type='l',ylim=c(0,20),col=1,lty=2,lwd=1)
    #       lines(smooth.spline(x = 1:(steps+1),y = confTSup[1:(steps+1),1,ii],spar = .50),type='l',ylim=c(0,20),col=1,lty=2,lwd=1)
    #       #Strategy 2
    #       lines(smooth.spline(x = 1:(steps+1),y =meanTS[1:(steps+1),2,ii],spar = .50),type='l',col=2,lty=1,lwd=2,ylim=c(0,20))
    #
    #       lines(smooth.spline(x = 1:(steps+1),y = confTSup[1:(steps+1),2,ii],spar = .50),type='l',ylim=c(0,20),col=2,lty=2,lwd=1)
    #       lines(smooth.spline(x = 1:(steps+1),y = confTSlow[1:(steps+1),2,ii],spar = .50),type='l',ylim=c(0,20),col=2,lty=2,lwd=1)
    #     }
    #   dev.off()
    
    #
    #
    # plotCI(x=jitter(rep(outdata[[4]][,var_par2_name],each=2)),meanTS[steps+1,,],uiw=sdTS[steps+1,,],pch=c(1,19),gap=0,ylim = c(0,max(confTSup)*1.1),minbar=0, ylab="Average population per patch",xlab= expression(Trade-off~strength~"("~italic(l)~")"), cex=1.4,main=paste0(condnameset[ss],"\n",seedvartypes[cc]," seed number"),col=rep(as.factor(outdata[[4]][,var_par1_name]),each=2))
    # # abline(h=0)
    
    
    #   }
    # }
    
    
    
    
    # Plot all mean respones for all parameters
    # par(mar=c(4,5.5,4,3),mfcol=c(2,2),cex.lab=1.2)
    # for(cc in 1:length(seedvartypes)){
    #   for(ss in 1:length(scenarios)){
    # plotCI(x=rep(outdata[[4]][,var_par2_name],each=1),meanTS[steps+1,2,],uiw=sdTS[steps+1,2,],pch=19,gap=0,ylim = c(0,max(confTSup)*1),minbar=0, ylab="Average population per patch",xlab= var_par2_name, cex=1.4,main=paste0(condnameset[ss],"\n",seedvartypes[cc]," seed number"),col=rep(as.factor(outdata[[4]][,var_par1_name]),each=2))
    # # abline(h=0)
    # for(vv in 1:length(var_par1)){
    #   lines(x=var_par2+(vv*.01),y=meanTS[steps+1,1,which(outdata[[4]][,var_par1_name]==var_par1[vv])],col=vv,lty=2)
    #   lines(x=var_par2+(vv*.01),y=meanTS[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],col=vv,lty=1)
    #   
    #   
    # }
    
    #plot each disturbance value separately
    for(vv in 1:length(var_par1)){
      #plot when non-neutral strategy is invader 
      # plotCI(x=rep(var_par2,each=1),meanTS_strat_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],uiw=sdTS_strat_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],pch=19,gap=0,minbar=0, ylab="Average population per patch",xlab= var_par2_name, cex=1.4,col=vv,main=paste0(var_par1_name,"=",var_par1[vv]),ylim=c(0,17))#main=paste0(condnameset[ss],"\n",seedvartypes[cc]," seed number"),#xlab= expression(Trade-off~strength~"("~italic(l)~")")#ylim = c(0,max(c(confTSup_strat_inv[,2,],confTSup_neut_inv[,2,]))*1),
      plotCI(x=rep(var_par2,each=1),meanTS_strat_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],uiw=sdTS_strat_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],pch=19,gap=0,minbar=0, ylab="",xlab= "", cex=1.4,col=vv,main="",ylim=c(0,17))#main=paste0(condnameset[ss],"\n",seedvartypes[cc]," seed number"),#xlab= expression(Trade-off~strength~"("~italic(l)~")")#ylim = c(0,max(c(confTSup_strat_inv[,2,],confTSup_neut_inv[,2,]))*1),
      # abline(h=0)
      # lines(x=var_par2+(vv*.01),y=meanTS_strat_inv[steps+1,1,which(outdata[[4]][,var_par1_name]==var_par1[vv])],col=vv,lty=2)
      lines(x=var_par2,y=meanTS_strat_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],col=vv,lty=1)
      abline(h=0)
      
      #######
      # Plot when neutral strategy is the invader
      par(new=TRUE)
      plotCI(x=rep(var_par2,each=1),meanTS_neut_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],uiw=sdTS_neut_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],pch=1,gap=0,minbar=0, ylab="",xlab= "",yaxt="n",xaxt="n", cex=1.4,col=vv,ylim=c(0,17))#ylim = c(0,max(c(confTSup_strat_inv[,2,],confTSup_neut_inv[,2,]))*1),
      # abline(h=0)
      # lines(x=var_par2+(vv*.01),y=meanTS_neut_inv[steps+1,1,which(outdata[[4]][,var_par1_name]==var_par1[vv])],col=vv,lty=2)
      lines(x=var_par2,y=meanTS_neut_inv[steps+1,2,which(outdata[[4]][,var_par1_name]==var_par1[vv])],col=vv,lty=1)
      abline(h=0)
      
      
      
    }
    
  }
}

# dev.off()



#################################
# Basic landscape plots
sidelength=50
hres=.2
hres=.8
pres=.2
pres=.8
prod=ideal.map(row = sidelength,col=sidelength,type = "fractal",range=(hres*sidelength/2)+.001,binmap=FALSE)
prodlim=quantile(prod,1-pres)
prod=(prod-prodlim)
prod=prod/max(prod)
prod[is.na(prod)]=0
prod[prod<=0]=0
prod=round(prod*maxprod)

# pdf("graphs/land_hres_2_pres_2.pdf",height=6.5)
# pdf("graphs/land_hres_2_pres_8.pdf",height=6.5)
# pdf("graphs/land_hres_8_pres_2.pdf",height=6.5)
# pdf("graphs/land_hres_8_pres_8.pdf",height=6.5)


image.plot(prod)
# dev.off()
