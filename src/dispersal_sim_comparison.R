# load libraries
# rm(list=ls(all=TRUE))
#library(fields)
library(snowfall)
# source("src/dispsim_func.R")

showtimes=seq(1,1000,by=4)


# Basic system parameters

steps=30
sidelength=30
totalcells=sidelength^2
speciesnum=2

landlevs=seq(0,1,length=3) #habitat heterogeneity, via Hyunt factor for clumpiness
biasvals=seq(0,.5,length=3) #flexibility in biasing seeds based on resources; 0 flexibility means seeds are always the same size, higher values means resources more strongly determine size:number ratio

seedprodvals=c(100) #seed production values for sp 2, sp 1 is set at 100
compvals= c(.95) #competition coefficient values for sp 2, sp 1 is set at 1
resvarlevs=c(.5,.75,1)#,1.25,1.5) #value that scales the variability in the resource landscape to more or less even than the standard created by altland. A non-spatial analaog to the variability produced from landlevs. 


inittype="block" #"block", "random" or "fractal"
blocksize=2 #Size of initial population block
blockpos="center" #"center" vs "corner"
hinit=.5 #Hyunt factor to determine clumpiness of initial populations
pinit=1 #proportion of habitat that has positive values of initial populations

resreplace="fractal" #"constant", "random" or "fractal"
maxprod=seq(400,200,length=4) #Resource production rate if constant, maximal production rate if quality varies

pres=.5 #proportion of habitat that has positive values of resource production for fractal resources
seed=11#random seed 

# r0s=c(1.2,1.3) #intrinsic rate of increase for each species as a vector
seedmort1=.1
seedmort2=.1
util1=1.1
util2=1.1
util=c(util1,util2) #average amount of resources used by an individual of each species
utildev=.1   #deviation in average amount of resources used by an individual of each species
mucoefs1=1
mucoefs2=1     
sigmacoefs1=10
sigmacoefs2=10
MPP=5 # Meters per pixel

serial=TRUE
if(serial==TRUE){
    cpunum=1
  }else{
    source("src/disperser_func.R")
    cpunum=6
  }
plotflag=TRUE
###############

parset=expand.grid(steps=steps, sidelength=sidelength, speciesnum=speciesnum, inittype=inittype, blocksize=blocksize, blockpos=blockpos, hinit=hinit, pinit=pinit, resreplace=resreplace, maxprod=maxprod, hres=landlevs, pres=pres, resscaler=resvarlevs, seed=seed, MPP=MPP, seedprod=seedprodvals, seedmort1=seedmort1, seedmort2=seedmort2, compcoef2=compvals, util2=util2, utildev=utildev, biasvals=biasvals, mucoefs1=mucoefs1, mucoefs2=mucoefs2, sigmacoefs1=sigmacoefs1,sigmacoefs2=sigmacoefs2)

parset$reslim=parset$maxprod*3

landcomp=array(dim=c(steps+1,speciesnum,nrow(parset))) #an array that stores the average population across the landscape

domcomp=array(dim=c(nrow(parset),2)) #an array that stores the ratio of the different species averaged across the landscape [,,1] and the total plant population size[,,2]



ptm=proc.time()
if(serial==FALSE){sfInit(parallel=TRUE,cpus=cpunum)}
##############        
qq=0
for (pp in 1:nrow(parset)){
qq=qq+1  
steps=parset[pp,"steps"]
sidelength=parset[pp,"sidelength"]
speciesnum=parset[pp,"speciesnum"]
inittype=parset[pp,"inittype"] #"block", "random" or "fractal"
blocksize=parset[pp,"blocksize"] #Size of initial population block
blockpos=parset[pp,"blockpos"]#"center" #"center" vs "corner"
hinit=parset[pp,"hinit"] #Hyunt factor to determine clumpiness of initial populations
pinit=parset[pp,"pinit"] #proportion of habitat that has positive values of initial populations

resreplace=parset[pp,"resreplace"] #"constant", "random" or "fractal"
maxprod=parset[pp,"maxprod"] #Resource production rate if constant, maximal production rate if quality varies
hres=parset[pp,"hres"] #Hyunt factor to determine clumpiness of resource landscape [0,1] for fractal resources
pres=parset[pp,"pres"] #proportion of habitat that has positive values of resource production for fractal resources
resscaler=parset[pp,"resscaler"] #value that scales the variability in the resource landscape to more or less even than the standard created by altland
seed=parset[pp,"seed"]#random seed 

# r0s=c(1.2,1.3) #intrinsic rate of increase for each species as a vector
seedprod=c(100,parset[pp,"seedprod"]) #seed production for each species as a vector
seedmort=c(parset[pp,"seedmort1"],parset[pp,"seedmort2"]) #natural pre-establishment mortality, without competition
compcoefs=c(1,parset[pp,"compcoef2"]) #relative competitiveness coefficients for establishment 
util1=1
util2=parset[pp,"util2"]
util=c(util1,util2) #average amount of resources used by an individual of each species
utildev=parset[pp,"utildev"] #deviation in average amount of resources used by an individual of each species
biasflex=c(0,parset[pp,"biasvals"])
mucoefs1=parset[pp,"mucoefs1"]
mucoefs2=parset[pp,"mucoefs2"]     
mucoefs=c(mucoefs1,mucoefs2)
sigmacoefs1=parset[pp,"sigmacoefs1"]
sigmacoefs2=parset[pp,"sigmacoefs2"]
sigmacoefs=c(sigmacoefs1,sigmacoefs2)
MPP=parset[pp,"MPP"] # Meters per pixel
reslim=parset[pp,"reslim"] #limit to possible resources

# serial=parset[pp,"serial"] #run in serial
# cpunum=parset[pp,"cpunum"] #number of processors for parallel run
# plotflag=plotflag #number of processors for parallel run






# Basic system parameters




spmeans=dispsim(steps=steps, sidelength=sidelength, speciesnum=speciesnum, inittype=inittype, blocksize=blocksize, blockpos=blockpos, hinit=hinit, pinit=pinit, resreplace=resreplace, maxprod=maxprod, hres=hres, pres=pres, resscaler=resscaler, seed=seed, seedprod=seedprod, seedmort=seedmort, compcoefs=compcoefs, util=util, utildev=utildev, biasflex=biasflex, mucoefs=mucoefs, sigmacoefs=sigmacoefs, MPP=MPP, reslim=reslim,serial=serial, cpunum=cpunum,plotflag=plotflag)  


landcomp[,,pp]=t(spmeans)
domcomp[pp,1]=spmeans[1,steps+1]/spmeans[2,steps+1]
domcomp[pp,2]=spmeans[1,steps+1]+spmeans[2,steps+1]
print(qq)
print(proc.time()-ptm)
}

# Rescale domcomp so that values go from -1, completely dominated by species 1, to +1, completely dominated by species 2
domcomprescale=1/domcomp[,1]
domcomprescale[which(domcomprescale<=1)]=domcomprescale[which(domcomprescale<=1)]-1
domcomprescale[which(domcomprescale>1)]=1-(1/domcomprescale[which(domcomprescale>1)])
print(proc.time()-ptm)
if(serial==FALSE){sfStop()}

# save(landcomp,file="data/landcomp.rdata")
# save(domcomprescale,file="data/domcomprescale.rdata")
# save.image(file="data/workdata.rdata")
